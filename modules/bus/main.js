"use strict";

var config = {
  apiLocation: 'http://10.0.1.2/pi_api/',
  buslineCount: 3,
}

var updateBuses = function(){
  var stop264;
  var stop662;
  $.when(
    $.get(config.apiLocation+'folistop/', {a: 'getStop', stop: 264}, function(response){
      stop264 = response.data;
    }, 'JSON'),
    $.get(config.apiLocation+'folistop/', {a: 'getStop', stop: 662}, function(response){
      stop662 = response.data;
    }, 'JSON')
  ).then(function(){
    var lines = [];
    lines.push({line: 'varsatie (264)', buses: stop264.slice(0, config.buslineCount)});
    lines.push({line: 'polttolaitoksenkatu (662)', buses: stop662.slice(0, config.buslineCount)});
    refreshBuses({lines: lines});
  });
  setTimeout(updateBuses, 60000);
};

var refreshBuses = function(data){
  $('#content-buses').empty();
  for (let line of data.lines) {
    var container = $('<div/>')
      .addClass('busline-container')
      .appendTo($('#content-buses'));
    container.append($('<h3/>').text(line.line));
    var ul = $('<ul/>')
      .addClass('buslines')
      .appendTo(container);
    for (let bus of line.buses) {
      var li = $('<li/>')
        .addClass('busline')
        .appendTo(ul);
      $('<span/>')
        .addClass('time')
        .text(bus.time)
        .appendTo(li); 
      $('<span/>')
        .addClass('line')
        .text(bus.line)
        .appendTo(li);
      $('<span/>')
        .addClass('destination')
        .text(bus.dest)
        .appendTo(li);
    }
  }
  window.parent.postMessage({action: "setSize", width: document.documentElement.offsetWidth, height: document.documentElement.offsetHeight}, "*");
};

updateBuses();
