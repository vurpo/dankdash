"use strict";

var config = {
};

var resizeCanvas = function() {
  var canvas = $('.graph-canvas');
  canvas.height = canvas.parent().offsetHeight;
  canvas.width = canvas.parent().offsetWidth;
};

window.addEventListener("message", function(event) {
  switch (event.data.action) {
    case "setSize":
      $('html')
        .height(event.data.height);
      resizeCanvas();
      break;
  };
});


window.onload = function() {
  new ResizeSensor($('.content'), function(){ 
    resizeCanvas();
  });

  window.parent.postMessage({action:"freeSize"}, "*");

  resizeCanvas();
};
