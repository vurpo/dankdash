"use strict";

var config = {
  octoPrintLocation: "http://10.0.1.3/",
  octoPrintAPIKey: "4E1A5A1DAFCF49BAA6A7814036FE6B06"
}

var updatePrinter = function() {
  var printing, percentage;
  
  $.when(
    $.ajax({url: config.octoPrintLocation+'api/job', headers: {'X-Api-Key': config.octoPrintAPIKey}, success: function(response) {
        percentage = response.progress.completion;
      }, dataType: 'JSON'}),
    $.ajax({url: config.octoPrintLocation+'api/printer', data: {exclude: 'temperature,sd'}, headers: {'X-Api-Key': config.octoPrintAPIKey}, success: function(response) {
        printing = response.state.text === "Printing";
      }, dataType: 'JSON'})
  ).then( function(job, printer) {
      refreshPrinter(true, printing, percentage);
      setTimeout(updatePrinter, 5000);
    }, function(job, printer) {
      refreshPrinter(false, null, null);
      setTimeout(updatePrinter, 20000);
    }
  );
}

var refreshPrinter = function(online, printing, percentage) {
  if(!online) {
    $('#printer-status').html('3d printer is offline');
    $('#printer-icon').attr('class','offline');
  } else {
    if (printing) {
      $('#printer-status').html('current print '+percentage.toFixed(1)+'%');
      $('#printer-icon').attr('class','printing');
    } else {
      $('#printer-status').html('3d printer is idle');
      $('#printer-icon').attr('class','idle');
    }
  }
  window.parent.postMessage({action: "setSize", width: document.documentElement.offsetWidth, height: document.documentElement.offsetHeight}, "*");
}

updatePrinter();
