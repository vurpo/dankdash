"use strict";

var modules = ["3dprinter", "bus", "graph"];

window.addEventListener("message", function(event) {
  switch (event.data.action) {
    case "setSize":
      $(event.source.frameElement.parentElement).removeClass('freesize');
      event.source.frameElement.height = event.data.height;
      break;
    case "freeSize":
      $(event.source.frameElement.parentElement).addClass('freesize');
      event.source.postMessage({action:'setSize', height: event.source.frameElement.offsetHeight, width: event.source.frameElement.offsetWidth}, "*");
      break;
  }
}, false);

window.onload = function() {
  for (let module of modules) {
    var moduleContainer = $('<div/>')
      .attr('id', module+'-container')
      .addClass('module-container')
      .appendTo($('body'));

    $('<object/>')
      .addClass('module')
      .attr('data', 'modules/'+module+'/index.html')
      .appendTo(moduleContainer);

    console.log("loaded module "+module);
  }
};
